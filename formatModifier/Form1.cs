﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace formatModifier
{
    public partial class Form1 : Form
    {
        private string path;
        private FileInfo fInfo;
        private StreamReader sr;
        private StreamWriter sw;
        private StreamWriter sw2;
        private string[] fileName;
        private string file;

        public Form1()
        {
            InitializeComponent();
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog OF = new OpenFileDialog();
            OF.DefaultExt = "*.xls";
            OF.Filter = "텍스트(*.txt)|*.txt|모든 파일(*.*)|*.*";
            OF.FilterIndex = 1;
            if (OF.ShowDialog() == DialogResult.OK)
            {
                path = OF.FileName;
                fInfo = new FileInfo(@path);
                sr = fInfo.OpenText();
                fileName = path.Split('.');
                file = "";

                for (int i = 0; i < fileName.Length-1; i++)
                {
                    file = file + fileName[i];
                }                
                MessageBox.Show(file);
                //sw2 = new StreamWriter(file + "_modified_close.txt");
                //processLine();
                richTextBox1.Clear();
                richTextBox1.AppendText("file load complete : " + file + "\n");
            }            
        }
        private void processLine()
        {
            string line;
            string[] tmpLine;
            string[] time;
            int count = 0;
            int year = 0;            
            while (sr.Peek() > 0)
            {
                line = sr.ReadLine();
                if (!line.Contains('#'))
                {                    
                    tmpLine = line.Split('\t');
                    time = tmpLine[0].Split('-');

                    if (year == 0)
                    {
                        //sw = new StreamWriter(file + "_" + time[0] + "_modified.txt");
                        sw = new StreamWriter(file + "_" + time[0] + "_modified_open.txt");
                        sw2 = new StreamWriter(file + "_" + time[0] + "_modified_close.txt");
                    }


                    if (int.Parse(time[0]) != year)
                    {
                        sw.Close();
                        sw2.Close();
                        //sw = new StreamWriter(file + "_" + time[0] + "_modified.txt");
                        sw = new StreamWriter(file + "_" + time[0] + "_modified_open.txt");
                        sw2 = new StreamWriter(file + "_" + time[0] + "_modified_close.txt");
                        year = int.Parse(time[0]);
                    }
                    line = time[0] + " " + time[1] + " " + time[2] + " " + "00" + " " + "00" + " " + "00" + " ";
                    
                    if (tmpLine[1].Equals(""))
                    {
                        MessageBox.Show("error at line " + count + " 값 : " + tmpLine[1]);                     
                    }
                    else
                    {
                        sw.WriteLine(line + tmpLine[1]);
                        sw2.WriteLine(line + tmpLine[4]);
                    }
                    
                }
                count++;
            }            
            richTextBox1.AppendText("processing done!\n");
            sw.Close();
            //sw2.Close();
        }

        private void processLine2()
        {
            string line;
            string[] tmpLine;
            int count = 0;
            int year = 0;

            sw = new StreamWriter(file + "_modified_kospi.txt");
            sw2 = new StreamWriter(file + "_modified_kospi200.txt");

            while (sr.Peek() > 0)
            {
                line = sr.ReadLine();
                if (!line.Contains('#'))
                {
                    tmpLine = line.Split('\t');

                    //if (year == 0)
                    //{
                    //    sw = new StreamWriter(file + "_" + tmpLine[0] + "_modified_kospi.txt");
                    //    sw2 = new StreamWriter(file + "_" + tmpLine[0] + "_modified_kospi200.txt");
                    //}

                    //if (int.Parse(tmpLine[0]) != year)
                    //{
                    //    sw.Close();
                    //    sw2.Close();
                    //    sw = new StreamWriter(file + "_" + tmpLine[0] + "_modified_kospi.txt");
                    //    sw2 = new StreamWriter(file + "_" + tmpLine[0] + "_modified_kospi200.txt");
                    //    year = int.Parse(tmpLine[0]);
                    //}

                    line = tmpLine[0] + " " + tmpLine[1] + " " + tmpLine[2] + " " + tmpLine[4] + " " + tmpLine[5] + " " + tmpLine[6] + " ";

                    if (tmpLine[7].Equals(""))
                    {
                        MessageBox.Show("error at line " + count + " : " + line);
                    }
                    else
                    {
                        if(tmpLine[3].Equals("01"))
                        {
                            sw.WriteLine(line + tmpLine[7]);
                        }
                        else if (tmpLine[3].Equals("28"))
                        {
                            sw2.WriteLine(line + tmpLine[7]);
                        }
                    }
                }
                count++;
            }
            richTextBox1.AppendText("processing done!\n");
            sw.Close();
            sw2.Close();
        }
        private void ribbonButton2_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText("begin process...\n");            
            processLine();
        }

        private void ribbonButton3_Click(object sender, EventArgs e)
        {
            processLine2();
        }

        private void ribbonButton4_Click(object sender, EventArgs e)
        {
            processLine3();
        }

        private void processLine3()
        {
            string[] txtLine;
            string[] hour;
            string line, year, month, day;

            int count = 0;

            sw = new StreamWriter(file + "_modified_open.txt");
            sw2 = new StreamWriter(file + "_modified_close.txt");

            while (sr.Peek() > 0)
            {
                line = sr.ReadLine();
                if (!line.Contains('#'))
                {
                    txtLine = line.Split('\t');
                    year = txtLine[0].Substring(0, 4);
                    month = txtLine[0].Substring(4, 2);
                    day = txtLine[0].Substring(6, 2);
                    hour = txtLine[1].Split(':');

                    line = year + " " + month + " " + day + " " + hour[0] + " " + hour[1] + " ";
                    if (txtLine[2].Equals("") || txtLine[3].Equals(""))
                    {
                        MessageBox.Show("error at line " + count + " : " + line);
                    }
                    else
                    {
                        sw.WriteLine(line + txtLine[2]);
                        sw2.WriteLine(line + txtLine[3]);
                    }
                }
                count++;
            }
            richTextBox1.AppendText("processing done!\n");
            sw.Close();
            sw2.Close();
        }

        private void ribbonButton5_Click(object sender, EventArgs e)
        {
            processLine4();
        }
        private void processLine4()
        {            
            string[] txtLine;
            string line;

            sw = new StreamWriter(file + "_arranged.txt");
            while (sr.Peek() > 0)
            {
                line = sr.ReadLine();
                txtLine = line.Split('\t');
                line = txtLine[0] + " " + txtLine[1];
                sw.WriteLine(line);
            }
            sw.Close();
        }
    }
}
